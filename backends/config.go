package backends

const (
	DefaultDataDir = "/otp/rssd/data"
)

type Config struct {
	Backend string
	DataDir string
}
