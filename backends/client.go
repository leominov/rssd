package backends

import (
	"errors"

	"github.com/leominov/rssd/backends/jsonfile"
)

type StoreClient interface {
	Load(key string, vars interface{}) error
	Save(key string, vars interface{}) error
}

func New(config Config) (StoreClient, error) {
	if config.Backend == "" {
		config.Backend = "json-file"
	}

	switch config.Backend {
	case "json-file":
		return jsonfile.NewFileClient(config.DataDir)
	}
	return nil, errors.New("Invalid backend")
}
