package log

import (
	"fmt"
	"os"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"
)

type RssdFormatter struct {
}

func (c *RssdFormatter) Format(entry *log.Entry) ([]byte, error) {
	timestamp := time.Now().Format(time.RFC3339)
	hostname, _ := os.Hostname()
	return []byte(fmt.Sprintf("%s %s %s[%d]: %s %s\n", timestamp, hostname, tag, os.Getpid(), strings.ToUpper(entry.Level.String()), entry.Message)), nil
}

var tag string

func init() {
	tag = os.Args[0]
	log.SetFormatter(&RssdFormatter{})
}

func SetTag(t string) {
	tag = t
}

func SetLevel(level string) {
	lvl, err := log.ParseLevel(level)
	if err != nil {
		Fatal(fmt.Sprintf(`not a valid level: "%s"`, level))
	}
	log.SetLevel(lvl)
}

func Debug(format string, v ...interface{}) {
	log.Debug(fmt.Sprintf(format, v...))
}

func Error(format string, v ...interface{}) {
	log.Error(fmt.Sprintf(format, v...))
}

func Fatal(format string, v ...interface{}) {
	log.Fatal(fmt.Sprintf(format, v...))
}

func Info(format string, v ...interface{}) {
	log.Info(fmt.Sprintf(format, v...))
}

func Warning(format string, v ...interface{}) {
	log.Warning(fmt.Sprintf(format, v...))
}
