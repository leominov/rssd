package webhook

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

type Client struct {
	url       string
	userAgent string
	cli       *http.Client
	headers   map[string]string
	basicAuth *BasicAuth
}

func New(config Config) *Client {
	cli := &http.Client{
		Timeout: config.Timeout,
	}
	return &Client{
		url:       config.URL,
		userAgent: config.UserAgent,
		cli:       cli,
		headers:   config.Headers,
		basicAuth: config.BasicAuth,
	}
}

func (c *Client) Send(data interface{}) error {
	body, err := json.Marshal(data)
	if err != nil {
		return err
	}
	req, err := http.NewRequest("POST", c.url, bytes.NewBuffer(body))
	if err != nil {
		return err
	}
	if c.basicAuth != nil {
		req.SetBasicAuth(c.basicAuth.Username, c.basicAuth.Password)
	}
	req.Header.Set("User-Agent", c.userAgent)
	req.Header.Set("Content-Type", "application/json")
	if len(c.headers) != 0 {
		for key, value := range c.headers {
			req.Header.Add(key, value)
		}
	}
	resp, err := c.cli.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Error getting url: %s (%s)", c.url, resp.Status)
	}
	return nil
}
