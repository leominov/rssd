package webhook

import "time"

const (
	DefaultUserAgent = "Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/5.0)"
	DefaultTimeout   = 5 * time.Second
)

type Config struct {
	Enabled   bool              `toml:"enabled"`
	UserAgent string            `toml:"user_agent"`
	URL       string            `toml:"url"`
	Timeout   time.Duration     `toml:"timeout"`
	Headers   map[string]string `toml:"headers"`
	BasicAuth *BasicAuth        `toml:"basic_auth"`
}

type BasicAuth struct {
	Username string `toml:"username"`
	Password string `toml:"password"`
}

func NewConfig() Config {
	return Config{
		UserAgent: DefaultUserAgent,
		Timeout:   DefaultTimeout,
		Headers:   make(map[string]string),
	}
}
