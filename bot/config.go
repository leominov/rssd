package bot

import "github.com/leominov/rssd/feeder"

type Config struct {
	Feeds         map[string]*feeder.Feed
	DataDir       string
	PidFile       string
	ListenAddress string
	MetricsPath   string
	PublicDir     string
	Dashboard     bool
}
