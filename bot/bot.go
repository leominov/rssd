package bot

type Bot interface {
	Process()
	Shutdown()
}
