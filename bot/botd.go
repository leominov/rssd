package bot

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/leominov/rssd/backends"
	"github.com/leominov/rssd/feeder"
	"github.com/leominov/rssd/log"
	"github.com/leominov/rssd/telegram"
	"github.com/leominov/rssd/webhook"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type Botd struct {
	telegram    *telegram.Client
	webhook     *webhook.Client
	storeClient backends.StoreClient
	config      Config
	feeder      *feeder.Feeder
	stopChan    chan bool
	doneChan    chan bool
	errChan     chan error
}

func NewBotd(telegram *telegram.Client, webhook *webhook.Client, config Config, storeClient backends.StoreClient, stopChan, doneChan chan bool, errChan chan error) Bot {
	return &Botd{
		telegram:    telegram,
		webhook:     webhook,
		storeClient: storeClient,
		config:      config,
		stopChan:    stopChan,
		doneChan:    doneChan,
		errChan:     errChan,
	}
}

func (b *Botd) realProcess() {
	go b.serve()
	b.feeder = feeder.New(b.config.Feeds, b.storeClient, b.telegram, b.webhook)
	if err := b.feeder.Start(); err != nil {
		log.Error(err.Error())
	}
}

func (b *Botd) serve() {
	if b.config.Dashboard {
		http.HandleFunc("/feeds", b.FeedsHandler)
		http.Handle("/", http.FileServer(http.Dir(b.config.PublicDir)))
	}
	http.Handle(b.config.MetricsPath, promhttp.Handler())
	b.errChan <- http.ListenAndServe(b.config.ListenAddress, nil)
}

func (b *Botd) FeedsHandler(w http.ResponseWriter, r *http.Request) {
	var feeds []feeder.Feed
	for _, feedRaw := range b.feeder.Feeds {
		feed := *feedRaw
		feed.Items = nil
		feeds = append(feeds, feed)
	}
	encoder := json.NewEncoder(w)
	err := encoder.Encode(feeds)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (b *Botd) Process() {
	defer close(b.doneChan)

	if err := b.storePid(); err != nil {
		log.Fatal(err.Error())
		return
	}

	b.realProcess()
}

func (b *Botd) realShutdown() {
	b.feeder.Stop()
}

func (b *Botd) Shutdown() {
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		b.realShutdown()
	}()
	doneCh := make(chan struct{})
	go func() {
		wg.Wait()
		close(doneCh)
	}()
	select {
	case <-doneCh:
	case <-time.After(3 * time.Second):
		log.Fatal("Timeout")
	}

	if err := b.deletePid(); err != nil {
		log.Fatal(err.Error())
		return
	}

	log.Info("Bye")
}

func (b *Botd) storePid() error {
	pidPath := b.config.PidFile
	if pidPath == "" {
		return nil
	}

	pidFile, err := os.OpenFile(pidPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0666)
	if err != nil {
		return fmt.Errorf("Could not open pid file: %v", err)
	}
	defer pidFile.Close()

	pid := os.Getpid()
	_, err = pidFile.WriteString(fmt.Sprintf("%d", pid))
	if err != nil {
		return fmt.Errorf("Could not write to pid file: %s", err)
	}
	return nil
}

func (b *Botd) deletePid() error {
	pidPath := b.config.PidFile
	if pidPath == "" {
		return nil
	}

	stat, err := os.Stat(pidPath)
	if err != nil {
		return fmt.Errorf("Could not remove pid file: %s", err)
	}

	if stat.IsDir() {
		return fmt.Errorf("Specified pid file path is directory")
	}

	err = os.Remove(pidPath)
	if err != nil {
		return fmt.Errorf("Could not remove pid file: %s", err)
	}
	return nil
}
