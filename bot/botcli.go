package bot

import (
	"github.com/leominov/rssd/log"
)

type BotCli struct {
	config   Config
	stopChan chan bool
	doneChan chan bool
	errChan  chan error
}

func NewBotCli(config Config, stopChan, doneChan chan bool, errChan chan error) Bot {
	return &BotCli{config, stopChan, doneChan, errChan}
}

func (b *BotCli) Process() {
	if len(b.config.Feeds) > 0 {
		for name, feed := range b.config.Feeds {
			feed.Setup(nil, nil, nil)

			f, err := feed.Get()
			if err != nil {
				log.Error("Feed \"%s\" got error: %v", name, err)
				continue
			}

			log.Info("Feed \"%s\" is OK", name)

			if len(f.Items) == 0 {
				log.Warning("No entries in \"%s\"", name)
				continue
			}
		}
	}
	close(b.doneChan)
}

func (b *BotCli) Shutdown() {
}
