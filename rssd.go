package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/leominov/rssd/backends"
	"github.com/leominov/rssd/bot"
	"github.com/leominov/rssd/lib"
	"github.com/leominov/rssd/log"
	"github.com/leominov/rssd/metrics"
	"github.com/leominov/rssd/telegram"
	"github.com/leominov/rssd/webhook"
)

func init() {
	lib.SeedMathRand()
	metrics.InitMetrics()
}

func main() {
	var (
		b              bot.Bot
		telegramClient *telegram.Client
		webhookClient  *webhook.Client
	)

	flag.Parse()
	if printVersion {
		fmt.Printf("rssd %s\n", Version)
		os.Exit(0)
	}
	if err := initConfig(); err != nil {
		log.Fatal(err.Error())
	}

	stopChan := make(chan bool)
	doneChan := make(chan bool)
	errChan := make(chan error, 10)

	switch {
	case config.Watch:
		log.Info("Starting rssd")

		if telegramConfig.Enabled {
			telegramClient = telegram.New(telegramConfig)
		}

		if webhookConfig.Enabled {
			webhookClient = webhook.New(webhookConfig)
		}

		storeClient, err := backends.New(backendsConfig)
		if err != nil {
			log.Fatal(err.Error())
		}

		b = bot.NewBotd(telegramClient, webhookClient, botConfig, storeClient, stopChan, doneChan, errChan)
	default:
		b = bot.NewBotCli(botConfig, stopChan, doneChan, errChan)
	}

	go b.Process()

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)
	for {
		select {
		case err := <-errChan:
			log.Error(err.Error())
		case s := <-signalChan:
			log.Info(fmt.Sprintf("Captured %v. Exiting...", s))
			close(doneChan)
		case <-doneChan:
			b.Shutdown()
			os.Exit(0)
		}
	}
}
