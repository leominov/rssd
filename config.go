package main

import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/leominov/rssd/backends"
	"github.com/leominov/rssd/bot"
	"github.com/leominov/rssd/feeder"
	"github.com/leominov/rssd/log"
	"github.com/leominov/rssd/metrics"
	"github.com/leominov/rssd/telegram"
	"github.com/leominov/rssd/webhook"
)

var (
	config         Config
	telegramConfig telegram.Config
	botConfig      bot.Config
	backendsConfig backends.Config
	webhookConfig  webhook.Config

	configFile        = ""
	defaultConfigFile = "/opt/rssd/rssd.toml"
	printVersion      bool
	logLevel          string
	telegramToken     string
	watch             bool
	silent            bool
	datadir           string
	backend           string
	pidfile           string
	listenAddress     string
	metricsPath       string
	publicDir         string
	dashboard         bool
)

type Config struct {
	LogLevel      string           `toml:"log_level"`
	Watch         bool             `toml:"watch"`
	DataDir       string           `toml:"data_dir"`
	Backend       string           `toml:"backend"`
	PidFile       string           `toml:"pid_file"`
	Feeds         map[string]*Feed `toml:"feeds"`
	Telegram      telegram.Config  `toml:"telegram"`
	ListenAddress string           `toml:"listen_address"`
	MetricsPath   string           `toml:"metrics_path"`
	PublicDir     string           `toml:"dashboard_path"`
	Webhook       webhook.Config   `toml:"webhook"`
	Dashboard     bool             `toml:"dashboard"`
}

type Feed struct {
	Enabled  bool   `toml:"enabled"`
	Endpoint string `toml:"endpoint"`
	Interval string `toml:"interval"`
	ChatID   string `toml:"chat_id"`
	Insecure bool   `toml:"insecure"`
	Tags     string `toml:"tags"`
}

func init() {
	flag.BoolVar(&printVersion, "version", false, "print version and exit")
	flag.StringVar(&configFile, "config-file", "", "the rssd config file")
	flag.StringVar(&logLevel, "log-level", "", "level which rssd should log messages")
	flag.StringVar(&telegramToken, "telegram-token", "", "Telegram API token to use")
	flag.BoolVar(&watch, "watch", false, "enable watch mode")
	flag.StringVar(&datadir, "datadir", "/otp/rssd/data", "rssd data directory")
	flag.StringVar(&backend, "backend", "json-file", "backend to use")
	flag.StringVar(&pidfile, "pid-file", "/otp/rssd/rssd.pid", "path to file to store PID")
	flag.StringVar(&listenAddress, "listen-address", ":9108", "address to listen on for web interface and metrics")
	flag.StringVar(&metricsPath, "metrics-path", "/metrics", "path under which to expose metrics")
	flag.StringVar(&publicDir, "public-dir", "./public", "directory with public files")
	flag.BoolVar(&dashboard, "dashboard", false, "enable dashboard")
}

func initConfig() error {
	if configFile == "" {
		if _, err := os.Stat(defaultConfigFile); !os.IsNotExist(err) {
			configFile = defaultConfigFile
		}
	}

	config = Config{
		Dashboard:     false,
		Backend:       "json-file",
		DataDir:       "/otp/rssd/data",
		PidFile:       "/otp/rssd/rssd.pid",
		ListenAddress: ":9108",
		MetricsPath:   "/metrics",
		PublicDir:     "./public",
		Telegram:      telegram.NewConfig(),
		Webhook:       webhook.NewConfig(),
	}

	if configFile == "" {
		log.Debug("Skipping rssd config file.")
	} else {
		log.Debug("Loading " + configFile)
		configBytes, err := ioutil.ReadFile(configFile)
		if err != nil {
			return err
		}
		_, err = toml.Decode(string(configBytes), &config)
		if err != nil {
			return err
		}
	}

	processEnv()

	processFlags()

	if config.LogLevel != "" {
		log.SetLevel(config.LogLevel)
	}

	telegramConfig = config.Telegram

	botConfig = bot.Config{
		Feeds:         map[string]*feeder.Feed{},
		DataDir:       config.DataDir,
		PidFile:       config.PidFile,
		ListenAddress: config.ListenAddress,
		MetricsPath:   config.MetricsPath,
		PublicDir:     config.PublicDir,
		Dashboard:     config.Dashboard,
	}

	backendsConfig = backends.Config{
		Backend: config.Backend,
		DataDir: config.DataDir,
	}

	webhookConfig = config.Webhook

	if webhookConfig.Enabled {
		if len(webhookConfig.URL) == 0 {
			return errors.New("Parameter webhook.url must be specified")
		}
	}

	totalFeeds := len(config.Feeds)
	disabledFeeds := 0
	if totalFeeds > 0 {
		for name, confFeed := range config.Feeds {
			if !confFeed.Enabled {
				disabledFeeds++
				continue
			}

			duration, err := time.ParseDuration(confFeed.Interval)
			if err != nil {
				return fmt.Errorf("Error parsing feeds.%s.interval: %v", name, err)
			}

			endpointURL, err := url.Parse(confFeed.Endpoint)
			if err != nil {
				return fmt.Errorf("Cant parse feed.%s.endpoint: %v", name, err)
			}

			f := &feeder.Feed{
				Name:        name,
				Enabled:     confFeed.Enabled,
				Endpoint:    confFeed.Endpoint,
				EndpointURL: endpointURL,
				Interval:    duration,
				Tags:        confFeed.Tags,
				ChatID:      confFeed.ChatID,
				Insecure:    confFeed.Insecure,
			}

			botConfig.Feeds[name] = f
		}
	}

	metrics.FeedsTotalCounter.Set(float64(totalFeeds))
	metrics.FeedsDisabledCounter.Set(float64(disabledFeeds))

	if config.Watch {
		log.Info("Backend set to " + config.Backend)
	}

	return nil
}

func processEnv() {
	token := os.Getenv("RSSD_TELEGRAM_TOKEN")
	if len(token) > 0 {
		config.Telegram.Token = token
	}
}

func processFlags() {
	flag.Visit(setConfigFromFlag)
}

func setConfigFromFlag(f *flag.Flag) {
	switch f.Name {
	case "log-level":
		config.LogLevel = logLevel
	case "telegram-token":
		config.Telegram.Token = telegramToken
	case "watch":
		config.Watch = watch
	case "datadir":
		config.DataDir = datadir
	case "pid-file":
		config.PidFile = pidfile
	case "listen-address":
		config.ListenAddress = listenAddress
	case "metrics-path":
		config.MetricsPath = metricsPath
	case "public-dir":
		config.PublicDir = publicDir
	case "dashboard":
		config.Dashboard = dashboard
	}
}
