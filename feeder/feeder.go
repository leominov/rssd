package feeder

import (
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/leominov/rssd/backends"
	"github.com/leominov/rssd/lib"
	"github.com/leominov/rssd/log"
	"github.com/leominov/rssd/metrics"
	"github.com/leominov/rssd/telegram"
	"github.com/leominov/rssd/webhook"
)

const pause = 100 * time.Millisecond

type Feeder struct {
	Feeds  map[string]*Feed `json:"items"`
	loaded bool             `json:"loaded"`

	storeClient  backends.StoreClient
	telegram     *telegram.Client
	webhook      *webhook.Client
	messageChan  chan *Message
	dumpChan     chan string
	completeChan chan string
	doneChan     chan bool
}

func New(feeds map[string]*Feed, storeClinet backends.StoreClient, telegram *telegram.Client, webhook *webhook.Client) *Feeder {
	return &Feeder{
		Feeds:        feeds,
		storeClient:  storeClinet,
		telegram:     telegram,
		webhook:      webhook,
		messageChan:  make(chan *Message, 100),
		dumpChan:     make(chan string, 100),
		doneChan:     make(chan bool),
		completeChan: make(chan string),
		loaded:       false,
	}
}

func (f *Feeder) CheckDups() error {
	for name, item := range f.Feeds {
		for sname, sitem := range f.Feeds {
			if item.Endpoint == sitem.Endpoint && item.Name != sitem.Name {
				return fmt.Errorf("URL duplicate: \"%s\" == \"%s\"", name, sname)
			}
		}
	}

	return nil
}

func (f Feeder) telegramSend(chatID, message string) {
	if !f.loaded {
		return
	}
	if f.telegram == nil {
		log.Info("S! New message (chat_id: %s): %q", chatID, message)
		return
	}
	log.Debug("New message (chat_id: %s): %q", chatID, message)
	metrics.MessagesTotalCounter.Inc()
	if err := f.telegram.Send(chatID, message); err != nil {
		metrics.MessagesFailCounter.Inc()
		log.Error(err.Error())
	}
}

func (f Feeder) webhookSend(message *Message) {
	if f.webhook == nil {
		log.Info("S! New webhook: %#v", message)
		return
	}
	log.Debug("New webhook: %#v", message)
	metrics.WebhooksTotalCounter.Inc()
	if err := f.webhook.Send(message); err != nil {
		metrics.WebhooksFailCounter.Inc()
		log.Error(err.Error())
	}
}

func (f Feeder) messageWatch() {
	var loadedItems, totalItems int
	totalItems = len(f.Feeds)

	for {
		select {
		case msg := <-f.messageChan:
			f.telegramSend(msg.ChatID, msg.Text)
			f.webhookSend(msg)
			time.Sleep(pause)
		case name := <-f.dumpChan:
			feed, ok := f.Feeds[name]
			if !ok {
				log.Error("Trying to dump nonexisting feed: %s", name)
				continue
			}
			if feed.Saved {
				continue
			}
			if err := f.SaveFeed(feed); err != nil {
				metrics.SaveFeedErrorCounter.Inc()
				log.Error(err.Error())
			}
		case name := <-f.completeChan:
			loadedItems++
			log.Debug("Feed \"%s\" loaded (%d/%d)", name, loadedItems, totalItems)
			if loadedItems == totalItems {
				f.loaded = true
				log.Info("Loading complete")
			}
		case <-f.doneChan:
			return
		}
	}
}

func (f Feeder) Start() error {
	var wg sync.WaitGroup

	if err := f.CheckDups(); err != nil {
		return err
	}

	if len(f.Feeds) == 0 {
		return errors.New("Feed list is empty")
	}

	go f.messageWatch()

	log.Info("Start loading feeds...")

	for _, feedItem := range f.Feeds {
		wg.Add(1)
		stateFeed := &Feed{}
		f.storeClient.Load(feedItem.Name, &stateFeed)
		if stateFeed != nil {
			feedItem.MergeWith(stateFeed)
		} else {
			stateFeed.StartDelay = lib.RandomStagger(feedItem.Interval)
		}
		feedItem.Setup(f.messageChan, f.dumpChan, f.completeChan)
		go func(item *Feed) {
			defer wg.Done()
			if err := item.Watch(); err != nil {
				log.Error(err.Error())
			}
		}(feedItem)
	}

	wg.Wait()

	return nil
}

func (f Feeder) ReCalculateStartDelay() {
	for _, feedItem := range f.Feeds {
		if time.Now().UTC().After(feedItem.ScheduledTime) {
			feedItem.StartDelay = lib.RandomStagger(feedItem.Interval)
			continue
		}
		feedItem.StartDelay = feedItem.ScheduledTime.Sub(time.Now())
	}
}

func (f Feeder) SaveFeed(feed *Feed) error {
	log.Info("Saving \"%s\" data to store...", feed.Name)
	feed.MarkAsSafe()
	err := f.storeClient.Save(feed.Name, feed)
	if err != nil {
		feed.MarkAsUnsafe()
		return err
	}
	feed.SavedTime = time.Now().UTC()
	return nil
}

func (f Feeder) Stop() error {
	close(f.doneChan)

	f.ReCalculateStartDelay()

	for name, feedItem := range f.Feeds {
		log.Debug(
			"Stats for \"%s\" pulls/errors/duration: %d/%d/%v",
			name,
			feedItem.Stats.PullCount,
			feedItem.Stats.ErrCount,
			feedItem.Stats.PullDuration,
		)
		if err := f.SaveFeed(feedItem); err != nil {
			metrics.SaveFeedErrorCounter.Inc()
			log.Error(err.Error())
		}
	}
	return nil
}
