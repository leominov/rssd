package feeder

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/leominov/rssd/lib"
	"github.com/leominov/rssd/log"
	"github.com/leominov/rssd/metrics"
	"github.com/mmcdole/gofeed"
)

const (
	StatePending State = "pending"
	StateWorking State = "working"
	StateFailed  State = "failed"
	StateEmpty   State = "empty"

	MinSuccessPulls = 2
)

type State string

type Feed struct {
	Name            string        `json:"name"`
	Enabled         bool          `json:"enabled"`
	State           State         `json:"state"`
	UpdateStateTime time.Time     `json:"update_state_time"`
	Endpoint        string        `json:"endpoint"`
	EndpointURL     *url.URL      `json:"endpoint_url"`
	Interval        time.Duration `json:"interval"`
	Tags            string        `json:"tags"`
	StartDelay      time.Duration `json:"start_delay"`
	Stats           Stats         `json:"stats"`
	Items           []*Item       `json:"items"`
	ScheduledTime   time.Time     `json:"scheduled_time"`
	ChatID          string        `json:"chat_id"`
	Saved           bool          `json:"saved_on_disk"`
	SavedTime       time.Time     `json:"saved_time"`
	Insecure        bool          `json:"insecure"`

	previousState State
	messageCh     chan *Message
	dumpCh        chan string
	completeCh    chan string
	parser        *gofeed.Parser
}

type Stats struct {
	PullCount    int           `json:"pull_count"`
	SuccessCount int           `json:"success_count"`
	ErrCount     int           `json:"err_count"`
	PullDuration time.Duration `json:"pull_duration"`
}

type Item struct {
	ID      string    `json:"id"`
	Link    string    `json:"link"`
	Title   string    `json:"title"`
	AddedAt time.Time `json:"added_at"`
}

type Message struct {
	CreateTime   time.Time `json:"create_time"`
	Text         string    `json:"text"`
	Title        string    `json:"title"`
	Link         string    `json:"link"`
	Tags         []string  `json:"tags"`
	FeedName     string    `json:"feed_name"`
	FeedEndpoint string    `json:"feed_endpoint"`
	ChatID       string    `json:"chat_id"`
}

func (f *Feed) Setup(messageCh chan *Message, dumpCh chan string, completeCh chan string) error {
	f.State = StatePending

	f.parser = gofeed.NewParser()

	if f.Insecure {
		transport := &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		}
		client := &http.Client{
			Transport: transport,
		}
		f.parser.Client = client
	}

	f.messageCh = messageCh
	f.completeCh = completeCh
	f.dumpCh = dumpCh

	if len(f.Items) == 0 {
		f.StartDelay = lib.RandomStagger(100 * time.Millisecond)
	}

	return nil
}

func (f *Feed) Get() (*gofeed.Feed, error) {
	return f.parser.ParseURL(f.Endpoint)
}

func (f *Feed) Read() (*gofeed.Feed, error) {
	f.Stats.PullCount++
	metrics.PullsTotalCounter.Inc()

	if f.Stats.PullCount == 1 {
		log.Debug("Checking data from %s...", f.Endpoint)
	} else {
		log.Debug("Pulling data from %s...", f.Endpoint)
	}

	timeStart := time.Now()
	feed, err := f.Get()
	if err != nil {
		f.Stats.ErrCount++
		metrics.PullsFailCounter.Inc()
	} else {
		f.Stats.SuccessCount++
	}

	f.Stats.PullDuration = time.Since(timeStart)

	return feed, err
}

func (f *Feed) IsItemExists(item *gofeed.Item) bool {
	ident := item.GUID
	if item.GUID == "" {
		ident = item.Link
	}
	for _, i := range f.Items {
		if i.ID == ident || i.Title == item.Title {
			return true
		}
	}
	return false
}

func (f *Feed) AddItem(item *gofeed.Item) {
	ident := item.GUID
	if item.GUID == "" {
		ident = item.Link
	}
	f.Items = append(f.Items, &Item{
		ID:      ident,
		Link:    item.Link,
		Title:   item.Title,
		AddedAt: time.Now().UTC(),
	})
}

func (f *Feed) feedItemToString(item *gofeed.Item) string {
	if len(f.Tags) > 0 {
		return fmt.Sprintf("%s\n%s\n\n%s", item.Title, f.Tags, item.Link)
	}
	return fmt.Sprintf("%s\n\n%s", item.Title, item.Link)
}

func (f *Feed) SetState(state State) {
	if f.State == state {
		return
	}
	f.previousState = f.State
	f.State = state
	f.UpdateStateTime = time.Now().UTC()
	f.MarkAsUnsafe()
}

func (f *Feed) MarkAsUnsafe() {
	f.Saved = false
}

func (f *Feed) MarkAsSafe() {
	f.Saved = true
}

func (f *Feed) Watch() error {
	dumpTicker := time.NewTicker(5 * time.Minute)
	next := time.After(f.StartDelay)
	source, err := f.Read()
	if err != nil {
		f.SetState(StateFailed)
		log.Error("Error with checking \"%s\": %+v", f.Name, err)
	}
	log.Debug("First pulling delay for \"%s\": %v", f.Name, f.StartDelay)
	f.completeCh <- f.Name
	for {
		select {
		case <-next:
			source, err = f.Read()
			if err != nil {
				log.Error("Error with pulling \"%s\": %+v", f.Name, err)
				f.SetState(StateFailed)
				next = time.After(f.NextPullDuration())
				continue
			}
			items := source.Items
			if len(items) == 0 {
				log.Warning("No entries in \"%s\"", f.Name)
				f.SetState(StateEmpty)
				next = time.After(f.NextPullDuration())
				continue
			}
			if f.State != StateWorking && f.State != StatePending {
				// State will be changed after process, only one newest message will be send
				log.Info("Welcome back: %s", f.Name)
			}
			changed := false
			for id, item := range items {
				if f.IsItemExists(item) {
					continue
				}
				f.AddItem(item)
				changed = true
				// If stage changed from non working – sends only latest item
				// If SuccessCount > MinSuccessPulls – sends all items
				if (f.State != StateWorking && id == 0) || (f.State == StateWorking && f.Stats.SuccessCount > MinSuccessPulls) {
					f.messageCh <- f.NewMessage(f.ChatID, item)
				}
			}
			if changed {
				f.MarkAsUnsafe()
			}
			f.SetState(StateWorking)
			next = time.After(f.NextPullDuration())
		case <-dumpTicker.C:
			f.dumpCh <- f.Name
		}
	}
}

func (f *Feed) NewMessage(chatID string, item *gofeed.Item) *Message {
	link := item.Link
	linkURL, err := url.Parse(item.Link)
	if err == nil {
		if linkURL.Scheme == "" {
			linkURL.Scheme = f.EndpointURL.Scheme
		}
		if linkURL.Host == "" {
			linkURL.Host = f.EndpointURL.Host
		}
		link = linkURL.String()
	} else {
		log.Warning("Cant parse url from feeds.%s '%s' for message: %v", f.Name, item.Link, err)
	}
	return &Message{
		FeedName:     f.Name,
		FeedEndpoint: f.Endpoint,
		CreateTime:   time.Now(),
		Text:         f.feedItemToString(item),
		Title:        item.Title,
		Tags:         strings.Split(f.Tags, " "),
		ChatID:       chatID,
		Link:         link,
	}
}

func (f *Feed) NextPullDuration() time.Duration {
	interval := lib.RandomStagger(f.Interval)
	f.ScheduledTime = time.Now().UTC().Add(interval)
	return interval
}

func (f *Feed) MergeWith(feed *Feed) {
	f.Items = feed.Items
	f.Stats = feed.Stats
	f.State = feed.State
	f.StartDelay = feed.StartDelay
	f.ScheduledTime = feed.ScheduledTime
	f.previousState = feed.State
}
