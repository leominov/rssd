package lib

import (
	"testing"
	"time"
)

func TestRandomStagger(t *testing.T) {
	dirA := RandomStagger(0)
	if dirA != 0 {
		t.Fatalf("expected %d, got %v", 0, dirA)
	}
	dirB := RandomStagger(time.Second)
	if dirB == 0 {
		t.Fatalf("expected duration, got %v", 0)
	}
}

func TestSeedMathRand(t *testing.T) {
	SeededSecurely = false
	SeedMathRand()
	if !SeededSecurely {
		t.Fatal("expected true, got false")
	}

	SeededSecurely = false
	SeedMathRand()
	if SeededSecurely {
		t.Fatal("expected false, got true")
	}
}
