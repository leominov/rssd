angular.module('myApp.controllers', [])

    .controller('WelcomeController', function ($scope, API) {
        $scope.feeds = [];

        $scope.start = function() {
            GetFeeds();
        }

        $scope.start();

        function GetFeeds() {
            API.getFeeds()
                .then(function (result) {
                    $scope.feeds = result.data;
                });
        }
    });
