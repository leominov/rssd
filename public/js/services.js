angular.module('myApp.services', [])

    .service('API', function ($http) {
        this.getFeeds = function () {
            return $http.get(
                '/feeds'
            );
        };
    });
