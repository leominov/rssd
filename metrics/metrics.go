package metrics

import (
	"sync"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/version"
)

const (
	namespace = "rssd"
)

var (
	once sync.Once

	PullsTotalCounter = prometheus.NewCounter(prometheus.CounterOpts{
		Name: prometheus.BuildFQName(namespace, "", "pulls_total_count"),
		Help: "How many pulls.",
	})

	PullsFailCounter = prometheus.NewCounter(prometheus.CounterOpts{
		Name: prometheus.BuildFQName(namespace, "", "pulls_fail_count"),
		Help: "How many failed pulls.",
	})

	MessagesTotalCounter = prometheus.NewCounter(prometheus.CounterOpts{
		Name: prometheus.BuildFQName(namespace, "", "messages_total_count"),
		Help: "How many messages are sent.",
	})

	MessagesFailCounter = prometheus.NewCounter(prometheus.CounterOpts{
		Name: prometheus.BuildFQName(namespace, "", "messages_fail_count"),
		Help: "How many messages are fail.",
	})

	WebhooksTotalCounter = prometheus.NewCounter(prometheus.CounterOpts{
		Name: prometheus.BuildFQName(namespace, "", "webhooks_total_count"),
		Help: "How many webhook requests are sent.",
	})

	WebhooksFailCounter = prometheus.NewCounter(prometheus.CounterOpts{
		Name: prometheus.BuildFQName(namespace, "", "webhooks_fail_count"),
		Help: "How many webhook requests are fail.",
	})

	FeedsTotalCounter = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: prometheus.BuildFQName(namespace, "", "feeds_total_count"),
		Help: "How many feeds in configuration.",
	})

	FeedsDisabledCounter = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: prometheus.BuildFQName(namespace, "", "feeds_disabled_count"),
		Help: "How many disabled feeds in configuration.",
	})

	SaveFeedErrorCounter = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: prometheus.BuildFQName(namespace, "", "save_feed_error_count"),
		Help: "How many errors while saving feed.",
	})
)

func InitMetrics() {
	once.Do(func() {
		prometheus.MustRegister(version.NewCollector("rssd"))

		prometheus.MustRegister(PullsTotalCounter)
		prometheus.MustRegister(PullsFailCounter)
		prometheus.MustRegister(MessagesTotalCounter)
		prometheus.MustRegister(MessagesFailCounter)
		prometheus.MustRegister(WebhooksTotalCounter)
		prometheus.MustRegister(WebhooksFailCounter)
		prometheus.MustRegister(FeedsTotalCounter)
		prometheus.MustRegister(FeedsDisabledCounter)
		prometheus.MustRegister(SaveFeedErrorCounter)
	})
}
