# rssd

## Installation

```
go get github.com/leominov/rssd
make
```

## Configuration

### Example

```
log_level = "info"
data_dir = "./data/"
pid_file = "./rssd.pid"
watch = true
listen_address = "127.0.0.1:9108"

# Dashboard
dashboard = true
public_dir = "./public"

# Prometheus metrics
metrics_path = "/metrics"

[telegram]
    enabled = true
    token = "token"
    chat_id = "chat_id"

[webhook]
    enabled = true
    url = "http://127.0.0.1:8080/webhook"

[feeds]
    [feeds.redditblog]
    enabled = true
    endpoint = "https://redditblog.com/feed/"
    tags = "#blog #reddit"
    interval = "8h"
```

## Metrics

Name     | Description 
---------|-------------
pulls_total_count | How many pulls
pulls_fail_count | How many failed pulls
messages_total_count | How many messages are sent
messages_fail_count | How many messages are fail
webhooks_total_count | How many webhook requests are sent
webhooks_fail_count | How many webhook requests are fail
feeds_total_count | How many feeds in configuration
feeds_disabled_count | How many disabled feeds in configuration
save_feed_error_count | How many errors while saving feed
